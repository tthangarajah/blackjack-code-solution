package codingtest.domain;

/**
 * This is the domain class that represents a card in a card game.
 */
public class Card {
		
	public enum Suit 
	{
		SPADES , HEARTS, DIAMONDS, CLUBS ;
		
		@Override
		public String toString() 
		{
			switch (this) {
				case SPADES		: return "Spades";
				case HEARTS		: return "Hearts";
				case DIAMONDS	: return "Diamonds";
				case CLUBS		: return "Clubs";
				default		: return "??";
			}
		}
	};
	
	public enum Face 
	{
		TWO(2), THREE(3), FOUR(4), FIVE(5), SIX(6), SEVEN(7), EIGHT(8), NINE(9),
		TEN(10), JACK(10), QUEEN(10), KING(10), ACE(11);
		
		private final int faceValue;
		
		private Face(int faceValue) 
		{
			this.faceValue = faceValue;
		}
		
		public int getFaceValue() 
		{
			return this.faceValue;
		}
		
		@Override
		public String toString() 
		{
	        switch (this) {
	            case TWO	: return "2"; 
	            case THREE	: return "3";
	            case FOUR	: return "4";
	            case FIVE	: return "5";
	            case SIX	: return "6";
	            case SEVEN	: return "7";
	            case EIGHT	: return "8";
	            case NINE	: return "9";
	            case TEN	: return "10";
	            case JACK	: return "Jack";
	            case QUEEN	: return "Queen";
	            case KING	: return "King";
	            case ACE	: return "Ace";
	            default		: return "??";
	        }
	    }
	}
	
	/*
	 * Suit value of card
	 */
	private final Suit suit;
	
	/*
	 * Face value of card
	 */
	private final Face face;
	
	public Card(Suit suit, Face face) 
	{
		this.suit = suit;
		this.face = face;
	}
	/**
	 * @return the suit
	 */
	public Suit getSuit() 
	{
		return suit;
	}

	/**
	 * @return the face
	 */
	public Face getFace() 
	{
		return face;
	}
	
	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() 
	{		
		return face.toString() +" of "+suit.toString();
	}	
	
}
