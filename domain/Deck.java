package codingtest.domain;

import java.util.Collections;
import java.util.Stack;

/**
 * This is the class that represents a deck of cards in a card game.
 */
public class Deck {
	public enum Shuffle 
	{
		DEFAULT, RIFFLE, PHAROAH;
		
		@Override
		public String toString() 
		{
			switch (this) {
				case DEFAULT	: return "Standard";
				case RIFFLE		: return "Riffle";
				case PHAROAH	: return "Pharoah/Faro";
				default			: return "??";
			}
		}
	}	
	
	private Stack<Card> cardPack = new Stack<Card>();
	
	/**
     * The number of Card Packs used for this Deck.
     */
    private int numberOfPacks;
	
    /**
     * Shuffle Algorithm used for this Deck.
     */
    private Shuffle shuffle;
    
    public Deck()
    {
        setNumberOfPacks(1);        
        setShuffleAlgorithm(Shuffle.DEFAULT);        
        createNewPack();
        shuffle();
    }

    public Deck(Shuffle shuffle, int numberOfPacks)
    {
        setNumberOfPacks(numberOfPacks);        
        setShuffleAlgorithm(shuffle);        
        createNewPack(); 
        shuffle();
    }
    
    /**
     * Mutator method that sets the number of Card Packs used for this deck.
     * @param number
     */
    private void setNumberOfPacks(int number)
    {
        this.numberOfPacks = number;
    }
    
    /**
     * returns the number of Card Packs used
     * for this deck.
     * @return
     */
    public int getNumberOfPacks()
    {
        return this.numberOfPacks;
    }

    /**
     * Mutator method that sets the shuffle algorithm that used for this deck.
     * @param number
     */
    private void setShuffleAlgorithm(Shuffle shuffle)
    {
        this.shuffle = shuffle;
    }
    
    /**
     * returns the shuffle algorithm of Card Packs used
     * for this deck.
     * @return
     */
    public String getShuffleAlgorithm()
    {
        return this.shuffle.toString();
    }
    
	/**
	 * Create a new pack
	 * @param numberOfPacks
	 */
	public void createNewPack() 
	{
		
		cardPack.clear();
		
		for (int i=0; i < numberOfPacks; i++) {
			for (Card.Suit suit : Card.Suit.values()){
				for(Card.Face face : Card.Face.values()){
					cardPack.add(new Card(suit, face));
				}
			}
		}	
	}
	
	/**
     * Shuffles the Deck of cards.
     */
    public void shuffle()
    {
    	switch (this.shuffle) {
    		case DEFAULT :
    			Collections.shuffle(cardPack);
    			break;
    		case RIFFLE	:
    			//TO DO
    			break;
			default:
				break;    			
    	}
        
    }
    
    /**
     * Deal the card from the Deck.
     */
    public Card deal()
    {
        if (cardPack.empty())
        {
            System.out.println("Run out of cards. New Deck.");
            
            createNewPack();
            shuffle();;
        }
        
        return cardPack.pop();
    }


}
