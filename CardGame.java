package codingtest;

import java.util.ArrayList;
import java.util.List;

import codingtest.domain.Card;
import codingtest.domain.Deck;

/**
 * Class that handles the playing of a card game from a simple command line interface,
 * and echoes back a step-by-step description of the game to the console.
 */
public class CardGame {

	
    /**
     * Main. Plays a card game from a command line interface.
     * @param args the arguments to the game
     */
    public static void main(String[] args) {
    	CardGame cardGame = new CardGame();
    	
    	int players = 3;
    	
    	if (args.length > 0) 
    	{
    		
    		try 
    		{
    			players = Integer.parseInt(args[0]);
    		}catch (Exception e) {}
    		
    		if (players < 1 && players > 6) {
    			players = 3;
    		}
    	}
    	cardGame.callTheGame(players);    	
    }
    
    public void callTheGame(int players) {
    	BlackJack balckJack = new BlackJack(players, Deck.Shuffle.DEFAULT);
    	balckJack.play();
    	System.out.println("*********************************************************");
    	System.out.println(balckJack.whoWon());
    	System.out.println("*********************************************************");
    }
    
    public void say(String msg) {
    	System.out.println(msg);
    }
    
    private class BlackJack {
    	private static final int CARD_PACKS = 1;
    	private static final String PLAYER_NAME_PREFIX = "Player";
    	
    	private final int noofPlayers;
    	
    	private List<PlayersHand> players = null;
    	private final Deck deck;
    	    	    	
    	public BlackJack(int noofPlayers, Deck.Shuffle shuffle) {
    		this.noofPlayers 	= noofPlayers;
    		
    		players 	= new ArrayList<PlayersHand>();
    		deck		= new Deck(shuffle, CARD_PACKS);
    		
    		say("Deck has been created");
    		say("Deck has been shuffled");
    	}
    	
    	private boolean isGameOver() {
    		int noofStick=0, noofBust=0;
    		
    		for(PlayersHand player : players){
    			if (player.hasBlackjack()) {
    				return true;
    			} else if(player.isBust()){
    				noofBust++;
    			} else if(player.isStick()){
    				noofStick++;    				
    			}
    		}
    		
    		return noofStick == this.noofPlayers || noofBust == this.noofPlayers -1;
    	}
    	
    	public String whoWon() {
    		
    		boolean isFinished = isGameOver();
    		
    		int noofStick=0;
    		int winTotal = 0;
    		StringBuffer result= null;
    		
    		for(PlayersHand player : players){    			
    			if (isFinished) {
	    			if (player.hasBlackjack()) {
	    				result = new StringBuffer(player.toString());
	    				result.append("\n"+player.getPlayerName()+" "+player.getStausDesc());
	    				break;
	    			} else if(player.isStick()){
	    				noofStick++;
	    				result = new StringBuffer(player.toString());
	    				result.append("\n"+player.getPlayerName()+" won the game");
	    			}
	    			
	    			if (noofStick == this.noofPlayers){
	        			result = new StringBuffer("All loose the game");
	        		}
    			} else if (player.isStick() && winTotal < player.getTotal()){
    					winTotal = player.getTotal();
    					result = new StringBuffer(player.toString());
    					result.append("\n "+player.getPlayerName()+" won the game");    				
    			}
    		}
    		
    		return result.toString();
    	}
    	
    	public void play() {
    		
    		//initial
    		say("Deals initial two cards to the each player");
    		
    		int i=0, j=0;
    		boolean repeat = true;
    		
    		while (i < 2 && repeat) { 
    			j=0;
    			while (j < noofPlayers && repeat) {
    				
    				Card deal = deck.deal();
    				if (i==0) {	
    					PlayersHand playerHand = new PlayersHand(PLAYER_NAME_PREFIX+(j+1));
	    				playerHand.add(deal);
	    				
	    				players.add(playerHand);
	    				
	    				say(playerHand.getPlayerName() +"-->"+deal.toString());
	    			} else {
	    				PlayersHand playerHand = players.get(j);
	    				playerHand.add(deal);	    				
	    				players.set(j, playerHand);
	    				
	    				say(playerHand.getPlayerName() +"-->"+deal.toString());
	    				
	    				if (isGameOver()) {
	    					repeat = false;
	    				}
    				}
    				j++;
    			}
    			i++;
    		}
    		
    		//hit the deal
    		if (!isGameOver()) {    			
    			say ("Player requests another card");
    			
    			for(PlayersHand player : players){
    				
    				if (player.isHit()) {
    					player = hit(player);
    				}
    				
    				say(player.getPlayerName()+" is "+player.getStausDesc());
    				
    				if (isGameOver()) {
        				break;
        			}
    			}    			
    		} 
    		
    		
    	}
    	
    	/**
         * Player requests another card.
         *
         * @param 
         */
        public PlayersHand hit(PlayersHand player)
        {
        	while (player.isHit()) 
        	{
        		Card deal = deck.deal();
        		player.add(deal);
        		
        		say("Hit-->"+deal.toString());
        	}            
        	return player;
        }

    	
	    private class PlayersHand 
	    {   
	    	private final String playerName;
	    	
	    	private int playersTotal;
	    	
	    	private List<Card> cards;
	    	
	    	public PlayersHand(String playerName) 
	    	{
	    		this.playerName = playerName;
	    		cards = new ArrayList<Card>();
	    	}
	    	
	        /**
	         * Clear the player hand. Remove all cards.
	         */
	        public void clear()
	        {
	        	cards.clear();
	        }
	        
	        /**
	         * Returns the player's name
	         * @return 
	         */
	        public String getPlayerName()
	        {  	            	            
	            return this.playerName;
	        }
	        
	        /**
	         * Returns the player hand total.
	         * @return 
	         */
	        public int getTotal()
	        {  
	            	            
	            return this.playersTotal;
	        }
	        
	        /**
	         * Checks whether card hand is bust or not.
	         * @return 
	         */
	        public boolean isBust()
	        {
	            return (playersTotal > 21) ? true : false;
	        }
	        
	        /**
	         * Checks whether card hand is stick or not.
	         * @return 
	         */
	        public boolean isStick()
	        {
	            return ( playersTotal >= 17  && playersTotal < 21) ? true : false;
	        }
	        
	        /**
	         * Checks whether card hand is hit or not.
	         * @return 
	         */
	        public boolean isHit()
	        {
	            return ( playersTotal >= 0  && playersTotal < 17) ? true : false;
	        }
	        
	        /**
	         * Check to see if hand has Blackjack - that being equal to 21 and 
	         * @return 
	         */
	        public boolean hasBlackjack()
	        {
	            return (playersTotal == 21) ? true : false;
	        }
	                        
	        /**
	         * Add a Card to the hand.
	         * @param   card    A card to add to the players hand.
	         * @return
	         */
	        public boolean add(Card card)
	        {
	            boolean cardAdded = false;
	            
	            if (isHit())
	            {            
	                cardAdded = cards.add(card);
	                
	                playersTotal += card.getFace().getFaceValue();
	            }
	            
	            return cardAdded;
	        }
	        
	        /**
	         * String representation of card hand.
	         * @return 
	         */
	        public String toString()
	        {
	        	StringBuffer handBuffer= new StringBuffer();
	        	
	        	for (Card eachCard : cards)
	            {
	        		handBuffer.append(eachCard.toString()+"\n");
	            }
	        	handBuffer.append("Player's total :"+getTotal()+"\n");
	            return handBuffer.toString();
	        }
	        
	        public String getStausDesc() {
	        	if (isBust()) {
	        		return "go bust";
	        	} else if (isStick()){
	        		return "stick";
	        	}else if (hasBlackjack()){
	        		return "won the game";
	        	} else {
	        		return "hit";
	        	}
	        }	        	
	    }
    }
}
